/*
 * マスタースレイブ制御プログラム
 *   
 * 2号機ではモーターボードは
 * 足回り１               ：0x02 (4wd host)
 * 足回り２               ：0x03 (4wd slave)
 * ベッド前後             ：0x04 
 * マスタースレイブ左右   ：0x05(このプログラム）
 * 
 * 
 */
#include <EEPROM.h>
#include <SPI.h>
#include <mcp_can.h>
//#include <MsTimer2.h>
#include <SerialCommand.h>
#include <motor.h>
#include <can_communication.h>

#define LIMIT_SW_L  INPUT1_1    //SWは通常導通、押したら切断
#define LIMIT_SW_R  INPUT1_2    //プルアップして使う(INPUT_PULLUP)。通常LOW<->押されたらHIGH
#define SENSOR_L    INPUT2_1
#define SENSOR_R    INPUT2_2
#define MOTOR_L     MOTOR1
#define MOTOR_R     MOTOR2

/*
 * motorWrite()の第二引数に使う。
 * motorWrite(MOTOR_R, R_FORWARD * 100); で右腕を前向きに速度100で動かす
 */
#define L_FORWARD   (1)
#define L_BACK      (-1)
#define R_FORWARD   (-1)
#define R_BACK      (1)

#define SENS_VAL_WIDTH    200

#define LIM_SW_L_PUSHED (digitalRead(LIMIT_SW_L) == HIGH)
#define LIM_SW_R_PUSHED (digitalRead(LIMIT_SW_R) == HIGH)

const uint8_t self_can_addr = 0x05;

const uint8_t COMMAND_UPDATE = 51;
const uint8_t COMMAND_STOP = 2;
const uint8_t COMMAND_FREE = 3;
const uint8_t COMMAND_RIGHT = 4;
const uint8_t COMMAND_LEFT = 5;

boolean p_ctrl_enabled = false;

/* 
 *  左右の目標位置。0からSENS_VAL_WIDTH
 *  0のときアームが一番手前
 */
uint16_t target_l;
uint16_t target_r;

/*
 * 原点
 * センサ値なので注意（target_と次元が違う）
 */
uint16_t origin_l;
uint16_t origin_r;

//比例係数
const uint16_t Kp = 10;

//目標値と現在値の許容する誤差
const int8_t margin = 5; 

SerialCommand SCmd;

// 各種移動実行関数
void command_left(int16_t lpos){
    if(lpos > SENS_VAL_WIDTH){
        target_l = SENS_VAL_WIDTH;
    }else if(lpos < 0){
        target_l = 0;
    }else{
        target_l = lpos;
    }

    if(p_ctrl_enabled){
        Serial.print("target_l:");
        Serial.println(target_l);
    }
}

void command_right(int16_t rpos){
    if(rpos > SENS_VAL_WIDTH){
        target_r = SENS_VAL_WIDTH;
    }else if(rpos < 0){
        target_r = 0;
    }else{
        target_r = rpos;
    }

    if(p_ctrl_enabled){
        Serial.print("target_r:");
        Serial.println(target_r);
    }
}

void wheelStop(){
    Serial.println("Stop");
    motorStop(MOTOR_L);
    motorStop(MOTOR_R); 
}

void wheelFree(){
    Serial.println("Free");
    motorFree(MOTOR_L);
    motorFree(MOTOR_R);
}

/*
// 1sごとに実行される
void timer2_tick(){
    digitalWrite(STAT_LED1, !digitalRead(STAT_LED1));
    CanCom.sendHeartBeat();    
}
*/

// データ受信時に呼ばれる関数
void canOnReceive(uint16_t std_id, const uint8_t *data, uint8_t len) {
    uint8_t type = CanCommunication::getDataTypeFromStdId(std_id);
    uint8_t src = CanCommunication::getSrcFromStdId(std_id);
    uint8_t dest = CanCommunication::getDestFromStdId(std_id);
    if(dest != self_can_addr){
      return;
      }
   Serial.println("Now we are in can on receive!");

    if(len < 1) return;
    if(dest == CAN_ADDR_BROADCAST) return;
    
    switch (data[0]) {
        case COMMAND_UPDATE:
            if(len == 5){
                int16_t r_pos = CanCommunication::get<int16_t>(&data[1]);
                int16_t l_pos = CanCommunication::get<int16_t>(&data[3]);
                Serial.print("r_pos :");
                Serial.println(r_pos);
                Serial.print("l_pos :");
                Serial.println(l_pos);
                command_right(r_pos);           
                command_left(l_pos);
            }else{
             Serial.println("CAN RECEIVE ERROR!"); 
             }
            break;

        case COMMAND_STOP:
            wheelStop();
            break;
        case COMMAND_FREE:
            wheelFree();
            break;
    }
    digitalWrite(STAT_LED2, !digitalRead(STAT_LED2));
}

//
// 以下PCとの通信用
//


void parseRight(){
    if(!p_ctrl_enabled){
        Serial.println("disabled");
        return;
    }
    
    char *arg = SCmd.next();
    bool err = false;
    if(arg != NULL){
        int16_t n = (int16_t)atoi(arg);
        command_right(n);
    } else parseError(NULL);
}

void parseLeft(){
    if(!p_ctrl_enabled){
        Serial.println("disabled");
        return;
    }
    
    char *arg = SCmd.next();
    if(arg != NULL){
        int16_t n = (int16_t)atoi(arg);
        command_left(n);
    } else parseError(NULL);
}

void parseStop(){
    uint8_t data[1] = { COMMAND_STOP };
    canOnReceive(
        CanCommunication::generateStdId(
            CAN_DATA_TYPE_COMMAND,
            self_can_addr,
            self_can_addr),
        data, 1);
    p_ctrl_enabled = false;
}

void parseFree(){
    uint8_t data[1] = { COMMAND_FREE };
    canOnReceive(
        CanCommunication::generateStdId(
            CAN_DATA_TYPE_COMMAND,
            self_can_addr,
            self_can_addr),
        data, 1);
}

void parsePos(){
    Serial.print("L:");
    Serial.println(analogRead(SENSOR_L));
    Serial.print("R:");
    Serial.println(analogRead(SENSOR_R));
}

void parseSetOrigin(){
    origin_l = analogRead(SENSOR_L);
    origin_r = analogRead(SENSOR_R);
    Serial.print("origin_l:");
    Serial.println(origin_l);
    Serial.print("origin_r:");
    Serial.println(origin_r);
}

void parseMoveRight(){
    int16_t n;
    char *arg = SCmd.next();
    bool err = false;
    if(arg != NULL){
        n = (int16_t)atoi(arg);
    } else err = true;
    if(!err){
        Serial.print("mr "); Serial.println(n);
        motorWrite(MOTOR2,R_FORWARD * n);
        delay(1000);
        motorStop(MOTOR2);
        Serial.print("current pos: ("); Serial.print(origin_l - analogRead(SENSOR_L)); Serial.print(", "); Serial.print(analogRead(SENSOR_R) - origin_r); Serial.println(")");
    } else parseError(NULL);
}

void parseMoveLeft(){
    int16_t n;
    char *arg = SCmd.next();
    bool err = false;
    if(arg != NULL){
        n = (int16_t)atoi(arg);
    } else err = true;
    if(!err){
        Serial.print("ml "); Serial.println(n);
        motorWrite(MOTOR1, L_FORWARD * n);
        delay(1000);
        motorStop(MOTOR1);
        Serial.print("current pos: ("); Serial.print(origin_l - analogRead(SENSOR_L)); Serial.print(", "); Serial.print(analogRead(SENSOR_R) - origin_r); Serial.println(")");
    } else parseError(NULL);
}

void parseReset(){
    Serial.println("Reset origin");
    resetOrigin();
}

void parseStopP(){
    p_ctrl_enabled = false;
    Serial.println("******** P-Control disabled ********");
}

void parseStartP(){
    target_l = origin_l - analogRead(SENSOR_L);
    target_r = analogRead(SENSOR_R) - origin_r;
    p_ctrl_enabled = true;
    Serial.println("******** P-Control enabled ********");
}

void parseError(const char *message){
    Serial.println("parse error");
}

void pCtrl(){
    //L
    int16_t curr_l = origin_l - analogRead(SENSOR_L);
    if(curr_l > SENS_VAL_WIDTH){
        motorWrite(MOTOR_L, L_BACK * 100);
        delay(500);
        motorStop(MOTOR_L);
        return;
    }
    
    int16_t err_l = target_l - curr_l;
    
    if(err_l > margin || err_l < -1 * margin){
        int tmp = L_FORWARD * Kp * err_l;
        motorWrite(MOTOR_L, tmp);
        //Serial.print("err_l:");
        //Serial.println(err_l);
    }else{
        motorStop(MOTOR_L);
        //Serial.println("in margin");
    }

    //R
    int16_t curr_r = analogRead(SENSOR_R) - origin_r;
    if(curr_r > SENS_VAL_WIDTH){
        motorWrite(MOTOR_R, R_BACK * 100);
        delay(500);
        motorStop(MOTOR_R);
        return;
    }
    
    int16_t err_r = target_r - curr_r;
    
    if(err_r > margin || err_r < -1 * margin){
        int tmp = R_FORWARD * Kp * err_r;
        motorWrite(MOTOR_R, tmp);
        //Serial.print("err_r:");
        //Serial.println(err_r);
    }else{
        motorStop(MOTOR_R);
    }
}

void resetOrigin(){
    motorStop(MOTOR_R);
    motorStop(MOTOR_L);
    
    //L
    if(!LIM_SW_L_PUSHED){
        motorWrite(MOTOR_L, L_BACK * 60);
        while(!LIM_SW_L_PUSHED);
        motorStop(MOTOR_L);
    }
    motorWrite(MOTOR_L, L_FORWARD * 60);
    while(LIM_SW_L_PUSHED);
    motorStop(MOTOR_L);
    origin_l = analogRead(SENSOR_L);

    //R
    if(!LIM_SW_R_PUSHED){
        motorWrite(MOTOR_R, R_BACK * 60);
        while(!LIM_SW_R_PUSHED);
        motorStop(MOTOR_R);
    }
    motorWrite(MOTOR_R, R_FORWARD * 60);
    while(LIM_SW_R_PUSHED);
    motorStop(MOTOR_R);
    origin_r = analogRead(SENSOR_R);

    //
    target_r = 0;
    target_l = 0;
}

void setup() {
    Serial.begin(115200);
    
    pinMode(STAT_LED1, OUTPUT);
    pinMode(STAT_LED2, OUTPUT);
    pinMode(LIMIT_SW_L,INPUT_PULLUP);
    pinMode(LIMIT_SW_R,INPUT_PULLUP);
    
    motorInit(); // モータ関連ポートも初期化される

    while(CanCom.begin(self_can_addr, CAN_250KBPS) != CAN_OK);
    CanCom.onReceive(canOnReceive); // 割り込み登録
    
    //MsTimer2::set(1000, timer2_tick);
    //MsTimer2::start();

    Serial.println("Hello");
    SCmd.addCommand("right", parseRight);
    SCmd.addCommand("left", parseLeft);
    SCmd.addCommand("stop", parseStop);
    SCmd.addCommand("free", parseFree);
    SCmd.addCommand("pos", parsePos);
    SCmd.addCommand("setorigin", parseSetOrigin);
    SCmd.addCommand("ml", parseMoveLeft);
    SCmd.addCommand("mr", parseMoveRight);
    SCmd.addCommand("stop_p", parseStopP);
    SCmd.addCommand("start_p", parseStartP);
    SCmd.addCommand("reset", parseReset);
    SCmd.setDefaultHandler(parseError);
    Serial.println("* * * Command * * *");
    Serial.println("* pos           :return current pos");
    Serial.println("* mr <spd>      :move right arm at <spd> speed for 1 second");
    Serial.println("* ml <spd>      :move left  arm at <spd> speed for 1 second");
    Serial.println("* setorigin     :set origin");
    Serial.println("* right <pos>   :move right arm to <pos>");
    Serial.println("* left <pos>    :move left  arm to <pos>");
    Serial.println("* stop          :stop motors and master control");
    Serial.println("* free          :free motors");
    Serial.println("* stop_p        :disable P-Control");
    Serial.println("* start_p       :enable P-Control");
    Serial.println("* reset         :auto-reset origin");
    Serial.println("* * *         * * *");

    delay(100);

    Serial.print("initializing arm position...");
    resetOrigin();
    Serial.println("done");

    p_ctrl_enabled = true;
    Serial.println("******** P-Control enabled ********");
}


void loop() {    
    SCmd.readSerial();
    CanCom.tasks();

    if(p_ctrl_enabled){
        pCtrl();
    }
}
