#include <SPI.h>

unsigned char data[6];

void setup() {
// SPI初期化
  pinMode(MISO,OUTPUT);
  SPI.setBitOrder(MSBFIRST);
  SPI.setClockDivider(SPI_CLOCK_DIV4);
  SPI.setDataMode(SPI_MODE0);
  // SPI通信を有効化する
  SPCR |= _BV(SPE);
  
  SPI.attachInterrupt();

//  Serial.begin(9600);
}

void loop() {
}

// 指定されたpinのアナログ値を0~255に正規化し返す
unsigned char getSensor(unsigned int pin) {
  // センサの最大値と最小値を登録
  static const unsigned int realLow[6] = { 585, 415, 1, 35, 1, 1};
  static const unsigned int realHigh[6] = { 409, 595, 1, 755, 1, 1};

  if (pin > 5) return 0;

  // 正規化
  long ld = map(analogRead(pin), realLow[pin], realHigh[pin], 0, 255);
  if (ld < 0) ld = 0;
  if (ld > 255) ld = 255;

  return (unsigned char) ld;
}

// 割り込み
ISR(SPI_STC_vect) {
    unsigned char pin = SPDR;
    static unsigned char data;
    data = getSensor(pin);
    SPDR = data;
//    Serial.println(data);
//    if (pin == 5)Serial.println();
}
