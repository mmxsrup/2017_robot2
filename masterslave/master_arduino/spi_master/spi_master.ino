#include <Joystick.h>

#include <Joystick.h>
#include <SPI.h>


/* SPI接続関係の設定 */
#include <SPI.h>
#define DATA_NUM 6
#define SSPin 10

/* HID関連の設定 */
#define JOYSTICK_COUNT 6 /* 受け取るアナログ値の数 */
#define ARM_RIGHT 0 
#define ARM_LEFT  1
#define HAND_RP   2
#define HAND_RY   3
#define HAND_LP   4
#define HAND_LY   5

Joystick_ Joystick[JOYSTICK_COUNT] = {
<<<<<<< HEAD
  Joystick_(0x03, JOYSTICK_TYPE_JOYSTICK, 1, 0, true, false, false, false, false, false, false, false, false, false, false),
  Joystick_(0x04, JOYSTICK_TYPE_JOYSTICK, 1, 0, true, false, false, false, false, false, false, false, false, false, false),
  Joystick_(0x05, JOYSTICK_TYPE_JOYSTICK, 1, 0, true, false, false, false, false, false, false, false, false, false, false),
  Joystick_(0x06, JOYSTICK_TYPE_JOYSTICK, 1, 0, true, false, false, false, false, false, false, false, false, false, false),
  Joystick_(0x07, JOYSTICK_TYPE_JOYSTICK, 1, 0, true, false, false, false, false, false, false, false, false, false, false),
  Joystick_(0x08, JOYSTICK_TYPE_JOYSTICK, 1, 0, true, false, false, false, false, false, false, false, false, false, false),
=======
// Example
/*Joystick(DEFAULT_REPORT_ID, TYPE, DEFAULT_BUTTON_COUNT, DEFAULT_HATSWITCH_COUNT, XAxis, YAxis, ZAxis, RxAxis, RyAxis, RzAxis, Rudder, Throttle, Accelerator, Brake, Steering)*/

  Joystick_(0x03, JOYSTICK_TYPE_JOYSTICK, 0, 0, true, true, true, true, true, true, false, false, false, false, false),
>>>>>>> 06ee1560e281226582535fdfaf309561ac654293
};


void setup() {
  /* I2C関係の初期設定 */
  pinMode(SSPin, OUTPUT);
  SPI.setBitOrder(MSBFIRST); /* 最上位bitから送信 */
  SPI.setClockDivider(SPI_CLOCK_DIV4); /* 通信速度をデフォルト */
  SPI.setDataMode(SPI_MODE0);
  SPI.begin();
  
  Serial.begin(9600); /* DEBUG */

  /* HID関係の初期設定 */
  for (int index = 0; index < JOYSTICK_COUNT; index++) {
    /* 0~255を-255~+255で-1~+1に正規化する */
    Joystick[index].setXAxisRange(-255, 255);
    Joystick[index].setYAxisRange(-255, 255); 
    Joystick[index].begin();
  }
}


void loop() {
  unsigned char buf;

  SetSSPin(LOW);
  
  /* DATA_NUM byte ずつもらう */
<<<<<<< HEAD
  for(int index = 0; index < DATA_NUM; index++) {
    buf = SPI.transfer((unsigned  char)index);
    Serial.println(buf);
    Joystick[(index + 5) % DATA_NUM].setXAxis(buf);
    delay(10);
  }

  /* ボタンを動かないと反応しないpcがあり, 追加 */
  for (int index = 0; index < DATA_NUM; index++) {
    Joystick[index].pressButton(0);
    Joystick[index].releaseButton(0);
  }
=======
  buf = SPI.transfer((unsigned  char)index);
  Serial.println(buf);
  Joystick[ARM_RIGHT].setXAxis(buf);
  Joystick[ARM_LEFT].setYAxis(buf);
  Joystick[HAND_RP].setZAxis(buf);
  Joystick[HAND_RY].setRxAxis(buf);
  Joystick[HAND_LP].setRyAxis(buf);
  Joystick[HAND_LY].setRzAxis(buf);
  delay(10);
>>>>>>> 06ee1560e281226582535fdfaf309561ac654293
  
  SetSSPin(HIGH);
  Serial.println();
  delay(100); /* slaveへのデータ要求間隔 */
}


/* SSピンの設定
 * LOW  でマスタからの通信有効 (セレクト)
 * HIGH でマスタからの通信できない (セレクト解除)
 */
void SetSSPin(int val) {
  digitalWrite(SSPin, val);
}

