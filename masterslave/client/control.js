var ws = new WebSocket("ws://192.168.21.150:8080", "control");

var state = {
    "mst" : {
        "arm" : {
            "r" : 0,
            "l" : 0,
        },
        "hand" : {
            "rP" : 0,
            "rY" : 0,
            "lP" : 0,
            "lY" : 0,
        },
    },
    "btn" : {
        "bed" : {
            "f" : 0,
            "b" : 0,
        },
        "sRes" : 0,
    },
}

function SendToServer(){
    ws.send(JSON.stringify(
        state,
    ));
}

function UpdateDisplay(){
    $("#armr").html(state.mst.arm.r);
    $("#arml").html(state.mst.arm.l);
    $("#handrp").html(state.mst.hand.rP);
    $("#handrY").html(state.mst.hand.rY);
    $("#handlP").html(state.mst.hand.lP);
    $("#handlY").html(state.mst.hand.lY);
    $("#bedf").html(state.btn.bed.f);
    $("#bedb").html(state.btn.bed.b);
    $("#sres").html(state.btn.sres);
}

function ParseData(data, index){
    // console.log(data, index);
    switch (index) {
        case 0 : 
            state.mst.arm.r = Math.ceil(data * 255.0); /* 整数値にする必要がある */
            break;
        case 1 : 
            state.mst.arm.l = Math.ceil(data * 255.0); /* 整数値にする必要がある */
            break;
        case 2 :
            state.mst.hand.rP = Math.ceil(data * 255.0);
            break;
        case 3 :
            state.mst.hand.rY = Math.ceil(data * 255.0);
            break;
        case 4 : 
            state.mst.hand.lP = Math.ceil(data * 255.0);
            break;
        case 5 : 
            state.mst.hand.lY = Math.ceil(data * 255.0);
            break;
    }
}

setInterval("UpdateDisplay()", 100);
setInterval("SendToServer()", 100);
