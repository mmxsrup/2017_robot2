
$(document).ready(function() {
	$("#arm").click(function() {
        leftval  = $("#leftarm").val();
        rightval = $("#rightarm").val();
        console.log("arm", leftval, rightval);
        ws.send(JSON.stringify({
        	"mst" : {
            	"arm" : {
                	"r" : rightval,
                	"l" : leftval,
            	},
        	},
    	}));
    })
    $("#4wd").click(function() {
    	leftval  = $("#left4wd").val();
        rightval = $("#right4wd").val();
        console.log("4wd", leftval, rightval);
        ws.send(JSON.stringify({
        	"4wd" : {
                go : {
                    left : leftval,
                    right : rightval,
                },
            }
    	}));	
    })
})

function arm(left, right){
	console.log(left, right);
	ws.send(JSON.stringify({
        "mst" : {
            "arm" : {
                "r" : right,
                "l" : left,
            },
        },
    }));
}