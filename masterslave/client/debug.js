var ws = new WebSocket("ws://192.168.21.150:8080", "control");

function KeyDownFunc(e){
    //input
    // キーコード
    var key_code = e.keyCode;
    // console.log(key_code);
    if(key_code == 87){//go
        console.log("mecanum");
        ws.send(JSON.stringify({
            mecanum: {
                go: {
                    dir: 1,
                    speed: 100
                },
            },
        }));
    }else if(key_code == 83){//back
        console.log("mst {arm, hand}");
        ws.send(JSON.stringify({
            "mst" : {
                "arm" : {
                    "r" : 30,
                    "l" : 0,
                },
            },
        }));
    }else if(key_code == 68){
        console.log("bed");
        ws.send(JSON.stringify({
            bed: {
                attitude: {
                    auto: false,
                    height: 500,
                    roll: 0,
                    pitch: 0
                },
                velt: {
                    go: 0,
                    roll: 0
                },
                "hand" : {
                    "rP" : 100,
                    "rY" : 100,
                    "lP" : 100,
                    "lY" : 100,
                },
            },
        }));
    }else if(key_code == 65){
        console.log("4WD");
        ws.send(JSON.stringify({
            "4wd" : {
                go : {
                    left : 100,
                    right : 100,
                },
            }
        }));
    }else if(key_code == 38){//上
        console.log("wd4");
        ws.send(JSON.stringify({
            wd4 : {
                lspeed : 100,
                rspeed : 100,
            },
        }));
    }else if(key_code == 40){
        console.log("sita");
        ws.send(JSON.stringify({
            pwm:{
                "1": 1000,
                "0": -1000,
            }
        }));
    }else{
        console.log("can");
        ws.send(JSON.stringify({
            can: {
                dest: 2,
                command: 1,
                arg1 :1000,
                arg2 :1000,
            },
        }));
    }
}

// イベントリスナーに対応している
if(document.addEventListener){

    // キーボードを押したときに実行されるイベント
    document.addEventListener("keydown" , KeyDownFunc);

}else{
    console.log("miss");
}