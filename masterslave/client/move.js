
move = {
	go : {
		left : 100,
		right : 100,
	},
	wd4 : {
        lspeed : 100,
        rspeed : 100,
    },
    can: {
        dest: 2,
        command: 1,
        arg1 :1000,
        arg2 :1000,
    },
}

move = {
	go : {
		left : 50,
		right : 50,
	},
	wd4 : {
        lspeed : 50,
        rspeed : 50,
    },
    can: {
        dest: 2,
        command: 1,
        arg1 :500,
        arg2 :500,
    },
}

function debug(){
    ws.send(JSON.stringify({
		wd4 : {
        	lspeed : 100,
        	rspeed : 100,
    	},
    	can: {
        	dest: 2,
        	command: 1,
        	arg1 :1000,
        	arg2 :1000,
    	},
    }));
}

function debug1(){
    ws.send(JSON.stringify({
        go : {
			left : 40,
			right : 40,
		},
		wd4 : {
        	lspeed : 40,
        	rspeed : 40,
    	},
    	can: {
        	dest: 2,
        	command: 1,
        	arg1 : 400,
        	arg2 : 400,
    	},
    }));
}

setInterval("debug()", 100);
setInterval("debug1()", 100);


