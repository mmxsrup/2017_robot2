var ws = new WebSocket("ws://192.168.2.100:8080", "control");

function KeyDownFunc(e){
    //input
    // キーコード
    var key_code = e.keyCode;
    // console.log(key_code);
    if(key_code == 87){//go
        ws.send(JSON.stringify({
            mecanum: {
                go: {
                    dir: 1,
                    speed: 100
                },
            },
        }));
    }else if(key_code == 83){//back
        ws.send(JSON.stringify({
            can: {
                dest: 2,
                command: 1,
                arg1 :-250,
                arg2 :-250,
            },
        }));
    }else if(key_code == 68){
        ws.send(JSON.stringify({
        }));
    }else if(key_code == 65){
        ws.send(JSON.stringify({
        }));
    }else if(key_code == 38){//上
        ws.send(JSON.stringify({
        }));
    }else if(key_code == 40){
        ws.send(JSON.stringify({
        }));
    }else{
        ws.send(JSON.stringify({
        }));
    }
}

// イベントリスナーに対応している
if(document.addEventListener){

    // キーボードを押したときに実行されるイベント
    document.addEventListener("keydown" , KeyDownFunc);

}else{
    console.log("miss");
}