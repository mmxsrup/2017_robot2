var server_ip = "";
var url = "ws://" + server_ip;
var camera_no = 0;
var camera_total_number = 2;

// 画像が飛んでくるプロトコル
var ws_camera_stream = new WebSocket(url, "camera-stream");

var objecturl;
var img = new Image;
var img_pre_w = 0, img_pre_h = 0;
ws_camera_stream.onmessage = function(evt) { //メッセージ受信イベント
    var data = evt.data;
    //受信
    if(typeof data === "string") {
        var data = JSON.parse(data);
        if("ip" in data) {
            $("#ip").html(data["ip"]);
        }
        if("camera" in data) {
            $("#camera_no").html(data["camera"]);
        }
        if("kbps" in data) {
            $("#com_speed").html(data["kbps"]);
        }
        if("status" in data) {
            $("#com_stat").html(data["status"]);
        }           
    } else  {           
        window.URL.revokeObjectURL(objecturl);
        objecturl = window.URL.createObjectURL(data);
        img.src = objecturl;
        img.onload = function () {
            var canvas = $("#img")[0];
            var c2d = canvas.getContext("2d");
            if (img_pre_h != img.height || img_pre_w != img.width) {
                c2d.clearRect(0, 0, canvas.width, canvas.height);
                img_pre_h = img.height;
                img_pre_w = img.width;
            }
            c2d.drawImage(img,0,0,img.width,img.height);
        };  
    }
};  
function changeCamera() {
    camera_no = (camera_no + 1) % camera_total_number;
    ws_camera_stream.send(JSON.stringify({
        camera: camera_no,
        get: true,
    }));
}

setInterval(function() {
    ws_camera_stream.send(JSON.stringify({get : true}));
}, 500);